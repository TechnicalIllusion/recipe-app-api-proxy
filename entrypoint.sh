#!/bin/sh

# This makes it so the shell script will stop if it encounters
# instead of failing and then continuing with the next command
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Makes it more Docker friendly, keeping the application at forefront
nginx -g 'daemon off;'